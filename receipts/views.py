from django.shortcuts import render, get_object_or_404, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
# Create your views here.


@login_required
def view_receipts(request):
    receipt = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipt_object": receipt
    }

    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid:
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid:
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("categories")
    else:
        form = CategoryForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid:
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("accounts")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/accounts/create.html", context)



@login_required
def view_categories(request):
    category = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "category_object": category,
    }

    return render(request, "receipts/categories.html", context)


@login_required
def view_accounts(request):
    account = Account.objects.filter(owner=request.user)

    context = {
        "account_object": account,
    }

    return render(request, "receipts/accounts.html", context)
